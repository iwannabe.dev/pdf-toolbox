import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from tkPDFViewer import tkPDFViewer
from PyPDF2 import PdfFileReader, PdfFileWriter
import copy


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.filepath = None
        self.pdf = None
        self.new_pdf = None
        self.rotation_degree = None

        self.title("PDF Toolbox")
        self.columnconfigure(index=0, minsize=100)
        self.geometry("850x900")
        self._create_menu()
        self.frame_submenu = tk.Frame(master=self, width=300, height=300, relief=tk.SUNKEN, border=3)
        self.frame_submenu.pack(side=tk.RIGHT)
        self.pdf_file_view = None
        self.load_pdf_file_view()


    def open_file(self):
        """Open a file for editing"""
        self.filepath = askopenfilename(
            filetypes=[("PDF Files", "*.pdf"), ("All Files", "*.*")]
        )

        if not self.filepath:
            return

        self.pdf = PdfFileReader(self.filepath)
        self.title(f"PDF Toolbox - {self.filepath}")
        self.load_pdf_file_view()


    def save_file(self):
        """Save to a file."""
        if not self.pdf:
            self.show_warning(title="Alert", message="You must open a file first.")
            return

        new_pdf = PdfFileWriter()
        new_pdf.appendPagesFromReader(self.pdf)
        self.filepath = asksaveasfilename(
            filetypes=[("PDF Files", "*.pdf"), ("All Files", "*.*")]
        )

        if not self.filepath:
            return

        with open(self.filepath, "wb") as output_file:
            new_pdf.write(output_file)

        self.title(f"PDF Toolbox - {self.filepath}")


    def load_pdf_file_view(self):
        if self.pdf_file_view:
            self.pdf_file_view.destroy()
        self.pdf_file = tkPDFViewer.ShowPdf()
        self.pdf_file.img_object_li.clear()
        self.pdf_file_view = self.pdf_file.pdf_view(
            master=self.frame_submenu,
            pdf_location=self.filepath,
        )

        self.pdf_file_view.pack()
        self.title(f"PDF Toolbox - {self.filepath}")


    def _create_menu(self):
        menu_bar = tk.Menu(master=self)
        self.config(menu=menu_bar)
        file_menu = tk.Menu(master=menu_bar)
        file_menu.add_command(label="Open file", command=self.open_file)
        file_menu.add_command(label="Save file", command=self.save_file)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=quit)
        menu_bar.add_cascade(label="File", menu=file_menu)
        edit_menu = tk.Menu(master=menu_bar)
        edit_menu.add_command(label="Split page(s)", command=self.split_page_options_window)
        edit_menu.add_command(label="Rotate page(s)", command=self.rotate_pages_options_window)
        edit_menu.add_command(label="Extract page(s)", command=self.extract_pages_options_window)
        menu_bar.add_cascade(label="Edit", menu=edit_menu)


    def show_warning(self, title="Alert", message="Something goes wrong."):
        tk.messagebox.showwarning(title=title, message=message)


    def rotate_pages(self):
        if self.options_window:
            page_numbers = self.entry.get()
            rotation_degree = self.rotation_degree.get()
            self.options_window.destroy()

        pages_to_rotate = page_numbers.split(",")
        new_pdf = PdfFileWriter()

        for page_no in pages_to_rotate:
            page = self.pdf.getPage(int(page_no) - 1)
            rotated_page = page.rotateClockwise(rotation_degree)
            new_pdf.addPage(rotated_page)

        temp_file = "temp_file.pdf"
        with open(temp_file, "wb") as output_file:
            new_pdf.write(output_file)

        self.filepath = temp_file
        self.pdf = PdfFileReader(self.filepath)
        self.load_pdf_file_view()


    def rotate_pages_options_window(self):
        if not self.pdf:
            self.show_warning(title="Alert", message="You must open a file first.")
            return

        self.options_window = tk.Toplevel()
        self.options_window.title("Page rotating options")

        label = tk.Label(
            master=self.options_window,
            text="Type in pages to be rotated (separated by comma and no spaces), ie. '2,3,5':"
        )
        label.pack(padx=10, pady=10)

        self.entry = tk.Entry(master=self.options_window)
        self.entry.pack()

        degree_values = {
            "90\N{DEGREE SIGN} clockwise": 90,
            "180\N{DEGREE SIGN} clockwise": 180,
            "270\N{DEGREE SIGN} clockwise": 270,
        }

        self.rotation_degree = tk.IntVar()
        for radiobutton_text, degree in degree_values.items():
            radiobutton = tk.Radiobutton(
                master=self.options_window,
                text=radiobutton_text,
                value=degree,
                variable=self.rotation_degree
            )
            radiobutton.pack()

        button = tk.Button(master=self.options_window, text="Apply", command=self.rotate_pages)
        button.pack(pady=10)

    def extract_pages(self):
        if self.options_window:
            user_input = self.entry.get()
            self.options_window.destroy()

        pages_to_extract = user_input.split(",")
        new_pdf = PdfFileWriter()

        for page_no in pages_to_extract:
            page = self.pdf.getPage(int(page_no)-1)
            new_pdf.addPage(page)

        temp_file = "temp_file.pdf"
        with open(temp_file, "wb") as output_file:
            new_pdf.write(output_file)

        self.filepath = temp_file
        self.pdf = PdfFileReader(self.filepath)
        self.load_pdf_file_view()


    def extract_pages_options_window(self):
        if not self.pdf:
            self.show_warning(title="Alert", message="You must open a file first.")
            return

        self.options_window = tk.Toplevel()
        self.options_window.title("Page extracting options")

        label = tk.Label(
            master=self.options_window,
            text="Type in pages to be extracted (separated by comma and no spaces), ie. '2,3,5':"
        )
        label.pack(padx=10, pady=10)

        self.entry = tk.Entry(master=self.options_window)
        self.entry.pack()

        button = tk.Button(master=self.options_window, text="Apply", command=self.extract_pages)
        button.pack(pady=10)


    def split_page(self):
        if self.sp_options_window:
            page_number = int(self.sp_entry.get()) - 1
            self.sp_options_window.destroy()

        choosen_page = self.pdf.getPage(page_number)
        pdf_page_left_side = copy.deepcopy(choosen_page)
        pdf_page_right_side = copy.deepcopy(choosen_page)

        current_coords = pdf_page_left_side.mediaBox.upperRight
        new_coords = (current_coords[0] / 2, current_coords[1])

        pdf_page_left_side.mediaBox.upperRight = new_coords
        pdf_page_right_side.mediaBox.upperLeft = new_coords

        self.new_pdf = PdfFileWriter()
        self.new_pdf.addPage(pdf_page_left_side)
        self.new_pdf.addPage(pdf_page_right_side)

        temp_file = "temp_file.pdf"
        with open(temp_file, "wb") as output_file:
            self.new_pdf.write(output_file)

        self.filepath = temp_file
        self.pdf = PdfFileReader(self.filepath)
        self.load_pdf_file_view()


    def split_page_options_window(self):
        if not self.pdf:
            self.show_warning(title="Alert", message="You must open a file first.")
            return

        self.sp_options_window = tk.Toplevel()
        self.sp_options_window.title("Page splitting options")

        sp_top_frame = tk.Frame(master=self.sp_options_window)
        sp_label_1 = tk.Label(
            master=sp_top_frame,
            text="Split a given page in half (vertically)."
        )
        sp_top_frame.grid()
        sp_label_1.grid(pady=5)

        sp_middle_frame = tk.Frame(master=self.sp_options_window)
        sp_label_2 = tk.Label(
            master=sp_middle_frame,
            text="Page number to split:"
        )
        sp_middle_frame.grid()
        sp_label_2.grid(row=0, column=0, sticky="e" ,padx=5, pady=5)

        self.sp_entry = tk.Entry(master=sp_middle_frame)
        self.sp_entry.grid(row=0, column=1, padx=5, pady=5)

        sp_bottom_frame = tk.Frame(master=self.sp_options_window)
        button = tk.Button(master=sp_bottom_frame, text="Apply", command=self.split_page)
        sp_bottom_frame.grid()
        button.grid(pady=5)


if __name__ == "__main__":
    app = App()
    app.mainloop()